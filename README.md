# Karla sucht den Kommunismus

Karla sucht den Kommunismus
------------------------------------------------

Eine kollektive interaktive Geschichte über das kommunistische
Begehren. 

Geschrieben in Python/Django.

Lizens: AGPL

Autor: Benni Bärmann