from django.db import models

class Description(models.Model):
    text = models.CharField(max_length=500)
    
    def __str__(self):
        return self.text
        
    
class Choice(models.Model):
    description = models.ForeignKey(Description, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    link = models.ForeignKey(Description, on_delete=models.CASCADE, related_name = 'links')
    
    def __str__(self):
        return self.text