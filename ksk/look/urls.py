from django.urls import path

from . import views

app_name = 'look'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:room_id>', views.room_change, name='room_change'),
]