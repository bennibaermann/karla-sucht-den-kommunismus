from django.contrib import admin

# Register your models here.
from .models import Choice, Description

admin.site.register(Description)
admin.site.register(Choice)
