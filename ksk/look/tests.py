from django.test import TestCase

from .models import Description, Choice

class QuestionModelTests(TestCase):
    
    def test_choices_have_descriptions(self):
        """
        make shure that ever Choice in the DB has links to existing
        Decriptions
        """
        d = Description.objects.all()
        
        for c in Choice.objects.all():
            self.assertIn(c.description, d)
            self.assertIn(c.link, d)
            
