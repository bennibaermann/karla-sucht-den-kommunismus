from django import forms

class LinkForm(forms.Form):
    choice_text = forms.CharField(label = 'Wahl Text:', max_length=100)
    link = forms.ModelChoiceField(queryset=None)
    

    # this constructor is necessar for a dynamic <select> tag
    def __init__(self, *args, **kwargs):
        
        queryset = kwargs.pop('rooms')
        super(LinkForm, self).__init__(*args,**kwargs)
        self.fields['link'].queryset = queryset
        
