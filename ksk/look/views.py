from django.shortcuts import render, get_object_or_404

# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse

from .models import Description, Choice
from .forms import LinkForm

# list of all avaiable rooms, should be admin-only in the finished game
def index(request):
    room_list = Description.objects.order_by('-id')[:5]
    context = {
        'room_list': room_list,
    }
    return render(request, 'look/index.html', context)
    
# room with form in a single view    
def room_change(request, room_id):
    
    room = get_object_or_404(Description,pk=room_id)
    room_list =  Description.objects.all()
    if request.method == 'GET':
        form = LinkForm(rooms = room_list)
    elif request.method == 'POST':
        form = LinkForm(request.POST, rooms = room_list)
        if form.is_valid():
            choice_text = form.cleaned_data['choice_text']
            d = Description.objects.get(id=room_id)
            next_room = form.cleaned_data['link']
            c = Choice( text = choice_text,
                        description = d,
                        link = next_room,
            )
            c.save()

            return HttpResponseRedirect(reverse('look:room_change',args=(room.id,)))

    else:
        raise "something went wrong!"
        
    context = {
        'form': form,
        'room': room,
        'choices': room.choice_set.all(),
        'room_list': room_list,
    }
    
    return render(request, 'look/room.html', context)
        
        
    
    